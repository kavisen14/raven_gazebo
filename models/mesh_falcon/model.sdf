<?xml version="1.0"?>
<sdf version="1.5">
  <model name="mesh_falcon">
    <!-- Mesh Falcon represents a stock falcon using a STL mesh
         provided by Paul for the visuals, and the same geometry
         as Block Falcon for the collisions.
    -->

    <!-- TODO: add color/texture to this model:
               http://gazebosim.org/tutorials?tut=guided_i2&cat=
	       the STL file doesn't include this information; look into DAE
    -->

    <link name="chassis">
      <pose>0 0 0 0 0 0</pose>

      <!-- For now, the falcon is modeled as a uniform-density rectangle;
            the thrusters aren't considered separately.-->
      <inertial>
        <mass>100.0</mass>
        <inertia>
          <ixx>0.051168</ixx>
          <iyy>0.104501</iyy>
          <izz>0.113333</izz>
          <ixy>0</ixy>
          <ixz>0</ixz>
          <iyz>0</iyz>
        </inertia>
      </inertial>

      <visual name="raven_mesh">
        <!-- The mesh was exported with:
             * the origin at the aft lower left corner
             * x/y/z as fwd/up/stbd.
             Rotate/translate into coordinate frame used by this model,
             where the origin is the geometric center of the vehicle
             and z is up.
             -->
        <pose frame="chassis"> -0.54 0.37 -0.26 1.57 0 0 </pose>
        <geometry>
          <mesh>
            <uri>model://mesh_falcon/meshes/Saab_Falcon.STL</uri>
            <!-- PG exported this at mm rather than meter scale. -->
            <scale> 0.001 0.001 0.001 </scale>
          </mesh>
        </geometry>
	<!-- Example taken from:
             uuv_simulator/uuv_gazebo_worlds/models/sand_heightmap/model.sdf
	     I haven't yet figured out how to get it to recognize the sand script
        -->
        <material>
          <script>
	    <!-- TODO: This neither fails to load nor properly colors the mesh.
	               Maybe the problem is the STL file?
		       ... I don't think so. This tutorial mixes STL + material:
		       https://uuvsimulator.github.io/packages/uuv_simulator/docs/tutorials/seabed_world/
            -->
	    <!-- relative to gazebo_media_path, exported in package.xml -->
            <uri>file://mesh_falcon/materials/scripts/sand.material</uri>
            <name>UUVSimulator/SandAndStones</name>
          </script>
          <emissive>
            0.6 0.6 0.6 1.0
          </emissive>
        </material>

      </visual>

      <collision name="collision_tophat_fwd">
        <!-- I really wish I could have variables here...-->
        <pose> 0.33 0 0.152</pose>
        <geometry>
          <box>
            <size> 0.34 0.56 0.2</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_tophat_aft">
        <!-- I really wish I could have variables here...-->
        <pose> -0.33 0 0.152</pose>
        <geometry>
          <box>
            <size> 0.34 0.56 0.2</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_stbd_upper">
        <pose> 0 0.29 0.152 </pose>
        <geometry>
          <box>
            <size> 1.0 0.02 0.2 </size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_port_upper">
        <pose> 0 -0.29 0.152 </pose>
        <geometry>
          <box>
            <size> 1.0 0.02 0.2 </size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_stbd_fwd">
        <pose> 0.475 -0.29 0</pose>
        <geometry>
          <box>
            <size> 0.05 0.02 0.504</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_port_fwd">
        <pose> 0.475 0.29 0</pose>
        <geometry>
          <box>
            <size> 0.05 0.02 0.504</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_stbd_aft">
        <pose> -0.475 -0.29 0</pose>
        <geometry>
          <box>
            <size> 0.05 0.02 0.504</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_port_aft">
        <pose> -0.475 0.29 0</pose>
        <geometry>
          <box>
            <size> 0.05 0.02 0.504</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_stbd_lower">
        <pose> 0 -0.29 -0.227</pose>
        <geometry>
          <box>
            <size> 1.0 0.02 0.05</size>
          </box>
        </geometry>
      </collision>

      <collision name="collision_port_lower">
        <pose> 0 0.29 -0.227</pose>
        <geometry>
          <box>
            <size> 1.0 0.02 0.05</size>
          </box>
        </geometry>
      </collision>

    </link>  <!-- End of Chassis link definition! -->


    <!-- The stock falcon has 5 thrusters. I don't know yet whether it makes
         more sense to have then as separate models/meshes, or as part of the
         overall robot mesh. (Depends on whether we expect their angles to be
         reconfigurable on our final build.)
         I assume that either way will be compatible with a generic thruster
         plugin + no-collision model.-->
    <!-- NOTE: The thrusters are really only for visualization, as the
         greensea interface doesn't allow us to control individual thruster
	 efford. So, go ahead and leave them off here b/c the thruster
	 model in teh block_falcon includes visual components that conflict
	 with the actual mesh. A more generally-useful Falcon model would
	 want those links to exist...
    -->

  </model>

</sdf>
